﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.DAL.Entities;

namespace WebAPI.DAL.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Task<IEnumerable<Category>> GetAll();
    }
}
