﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebAPI.DAL.Interfaces
{
    public interface IRepository<TEntity>
    {
        Task<TEntity> GetByIdAsync(int id);

        Task AddAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task DeleteByIdAsync(int id);
    }

}
