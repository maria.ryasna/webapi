﻿using System.Threading.Tasks;

namespace WebAPI.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        ICategoryRepository CategoryRepository { get; }

        IProductRepository ProductRepository { get; }

        Task<int> SaveAsync();

    }
}
