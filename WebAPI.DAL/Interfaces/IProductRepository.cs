﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.DAL.Entities;

namespace WebAPI.DAL.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<IEnumerable<Product>> GetAll(PaginationParams parameters);
    }
}
