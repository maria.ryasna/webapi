﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DAL.Entities;
using WebAPI.DAL.Interfaces;

namespace WebAPI.DAL.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly StoreDbContext _db;
        public ProductRepository(StoreDbContext context)
        {
            _db = context;
        }

        public async Task<IEnumerable<Product>> GetAll(PaginationParams parameters)
        {
            var products = await Task.Run(() => _db.Products);
            return products
                .Where(p => parameters.CategoryId == null || parameters.CategoryId == p.CategoryId)
                .Skip(parameters.PageNumber * parameters.PageSize)
                .Take(parameters.PageSize);
        }

        public async Task<Product> GetByIdAsync(int id)
        {
            return await _db.Products
                .Include(p => p.Category)
                .FirstOrDefaultAsync(p => p.ProductId == id);
        }

        public async Task AddAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            await _db.Products.AddAsync(product);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            await Task.Run(() => _db.Products.Update(product));
            await _db.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var product = await _db.Products.FindAsync(id);
            _db.Products.Remove(product);
            await _db.SaveChangesAsync();
        }
    }
}
