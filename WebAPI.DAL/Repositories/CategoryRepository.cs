﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.DAL.Entities;
using WebAPI.DAL.Interfaces;

namespace WebAPI.DAL.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly StoreDbContext _db;
        public CategoryRepository(StoreDbContext context)
        {
            _db = context;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await Task.Run(() => _db.Categories);
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            return await _db.Categories.FindAsync(id);
        }

        public async Task AddAsync(Category category)
        {
            if (category is null)
            {
                throw new ArgumentNullException(nameof(category));
            }
            await _db.Categories.AddAsync(category);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateAsync(Category category)
        {
            if (category is null)
            {
                throw new ArgumentNullException(nameof(category));
            }
            await Task.Run(() => _db.Categories.Update(category));
        }

        public async Task DeleteByIdAsync(int id)
        {
            var category = await _db.Categories.FindAsync(id);
            _db.Categories.Remove(category);
        }
    }
}
