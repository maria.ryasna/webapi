﻿namespace WebAPI.DAL.Entities
{
    public class PaginationParams
    {
        public int PageNumber { get; set; } = 0;
        public int PageSize { get; set; } = 10;
        public int? CategoryId { get; set; } = null!;
    }
}
