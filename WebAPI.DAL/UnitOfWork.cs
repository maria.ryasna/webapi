﻿using System;
using System.Threading.Tasks;
using WebAPI.DAL.Interfaces;
using WebAPI.DAL.Repositories;

namespace WebAPI.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly StoreDbContext _db;
        private ICategoryRepository _categoryRepository { get; }
        private IProductRepository _productRepository { get; }
        public ICategoryRepository CategoryRepository { get { return _categoryRepository; } }
        public IProductRepository ProductRepository { get { return _productRepository; } }

        public UnitOfWork(StoreDbContext db)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));
            _categoryRepository = new CategoryRepository(db);
            _productRepository = new ProductRepository(db);
        }
        public async Task<int> SaveAsync()
        {
            return await _db.SaveChangesAsync();
        }
    }
}
