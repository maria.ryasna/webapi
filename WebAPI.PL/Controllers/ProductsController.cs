﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebAPI.DAL.Entities;
using WebAPI.DAL.Interfaces;

namespace WebAPI.PL.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IUnitOfWork _uow { get; }
        public ProductsController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProductsAsync([FromQuery] PaginationParams parameters)
        {
            var products = await _uow.ProductRepository.GetAll(parameters);

            return Ok(products);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductByIdAsync([FromRoute] int id)
        {
            var products = await _uow.ProductRepository.GetByIdAsync(id);

            return Ok(products);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductByIdAsync([FromRoute] int id)
        {
            await _uow.ProductRepository.DeleteByIdAsync(id);

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProductAsync([FromRoute] int id, [FromBody] Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            product.ProductId = id;
            await _uow.ProductRepository.UpdateAsync(product);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddProductAsync([FromBody] Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            await _uow.ProductRepository.AddAsync(product);

            return Ok();
        }
    }
}
