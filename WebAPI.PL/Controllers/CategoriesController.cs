﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.DAL.Entities;
using WebAPI.DAL.Interfaces;

namespace WebAPI.PL.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private IUnitOfWork _uow { get; }
        public CategoriesController(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCategoriesAsync()
        {
            var categories = await _uow.CategoryRepository.GetAll();
            
            return Ok(categories);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategoryByIdAsync([FromRoute] int id)
        {
            var categories = await _uow.CategoryRepository.GetByIdAsync(id);

            return Ok(categories);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategoryByIdAsync([FromRoute] int id)
        {
            await _uow.CategoryRepository.DeleteByIdAsync(id);

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateCategoryAsync([FromBody] Category category)
        {
            await _uow.CategoryRepository.UpdateAsync(category);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddCategoryAsync([FromBody] Category category)
        {
            await _uow.CategoryRepository.AddAsync(category);

            return Ok();
        }
    }
}
